- **0.2.6** (2021-11-12):
    - Fix regression introduced in 0.2.5

- **0.2.5** (2021-11-12):
    - Cache already clipped extracts

- **0.2.4** (2021-11-12):
    - Fixed incomplete typing in cython parts

- **0.2.3** (2021-11-10):
    - Expose more classes

- **0.2.2** (2021-11-10):
    - Added missing dependency

- **0.2.1** (2021-11-07):
    - Fixed output filename handling

- **0.2.0** (2021-11-06):
    - Download and clip smallest possible extracts from Geofabrik or BBBike

- **0.1.0** (2021-11-05):
    - initial release
